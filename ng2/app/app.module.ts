import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";

import { AppComponent }  from './app.cmp';
import { routes } from './routes';
import { NewComponent } from './new/new.component';
import { SearchComponent } from './search/search.component';
import { ContactService } from './contact.service';

import { ContactFilterPipe } from './contact-filter.pipe';

@NgModule({
    imports: [ BrowserModule, HttpModule, FormsModule, RouterModule.forRoot(routes, { useHash: true })],
    declarations: [ AppComponent, NewComponent, SearchComponent, ContactFilterPipe ],
    providers: [ ContactService ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }