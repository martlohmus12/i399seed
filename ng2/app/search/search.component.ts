import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { Contact } from '../contact';


@Component({
    selector: 'list',
    templateUrl: 'app/search/search.component.html',
    styleUrls: ['app/search/search.component.css']
})
export class SearchComponent implements OnInit {
    contacts: Contact[] = [];


    constructor(private contactService: ContactService){

    }

    loadContacts(){
        this.contactService.getContacts()
            .then(contacts => this.contacts = contacts);
    }

    deleteContact(id){
        this.contactService.deleteContact(id)
            .then(() => this.loadContacts());
        
    }

    deleteContacts(){
        for(var i = 0; i < this.contacts.length; i++){
            if(this.contacts[i].selected){
                this.contactService.deleteContact(this.contacts[i]._id)
                    .then(() => this.loadContacts());
            }
            
        }
    }


    // selectAll() {
    //     for(var i = 0; i < this.contacts.length; i++){
    //         this.contacts[i].selected = true;
    //     }
    // }

    ngOnInit(): void {
        this.loadContacts();        
    }

}
