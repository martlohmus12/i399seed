import { Pipe, PipeTransform } from '@angular/core';
import { Contact } from './contact';

@Pipe({
    name: 'filter'
})
export class ContactFilterPipe implements PipeTransform {

   transform(value: any, term: string) {
        if (term) {
            term = term.toString().toLowerCase();
            return value.filter(function (contact: Contact) {
                var name = contact.name.toString();
                if(contact.phone != null){
                    name = name + contact.phone.toString();
                }
                return name.toLowerCase().indexOf(term) > -1;
            })
        }
        return value;
    }
}