import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { ContactService } from '../contact.service';
import { Contact } from '../contact';

@Component({
    templateUrl: 'app/new/new.html'
})
export class NewComponent implements OnInit {
    contact: Contact;
    newContactName: string;
    newContactPhone: string;
    updateMode: boolean = false;
  

    constructor(private route: ActivatedRoute,
                private router: Router,
                private contactService: ContactService) {}

    addNewContact(){
        if(this.updateMode){

            this.contact.name = this.newContactName;
            this.contact.phone = this.newContactPhone;

            this.contactService.updateContact(this.contact)
                .then(() => {
                    this.newContactName = '';
                    this.newContactPhone = '';
                    this.back();
                });

        }else{

            this.contactService.saveContact(new Contact(this.newContactName, this.newContactPhone))
                .then(() => {
                    this.newContactName = '';
                    this.newContactPhone = '';
                    this.back();
                });

        }
    }

    back() {
        this.router.navigate(['/search']);
    }

    loadContact(){
        var id = parseInt(this.route.snapshot.paramMap.get('id'));
            this.contactService.getContact(id)
                .then(contact => {
                        this.contact = contact;
                        this.newContactName = contact.name;
                        this.newContactPhone = contact.phone;
                    });        
    }

    ngOnInit(): void {
        var id = this.route.snapshot.paramMap.get('id');
        if(id != null){
            this.loadContact();
            this.updateMode = true;
        }
    }

}
