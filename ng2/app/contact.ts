export class Contact {
    _id: number;
    selected: boolean = false;
    constructor(
        public name: string,
        public phone: string
    ) {}
}