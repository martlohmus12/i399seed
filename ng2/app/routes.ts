import { Routes } from '@angular/router';

import { NewComponent } from './new/new.component';
import { SearchComponent } from './search/search.component';


export const routes: Routes = [
    { path: 'search', component: SearchComponent },
    { path: 'new', component: NewComponent },
    { path: 'new/:id', component: NewComponent },
    { path: '', redirectTo: 'search', pathMatch: 'full' }
];