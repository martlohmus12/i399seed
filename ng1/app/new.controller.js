(function () {
    'use strict';
    
    angular.module('app').controller('NewContactCtrl', Ctrl);

    function Ctrl($http, $location, $routeParams){
        var vm = this;
        this.newContact = new contact ('', '');
        this.addContact = addContact;
        this.edit = false;
        
        show();
        
        function contact(name, phone){
            this.name = name;
            this.phone = phone;
        }

        function addContact(){
            if($routeParams.id !== undefined) {
                $http.put('api/contacts/' + $routeParams.id, vm.newContact)
                    .then(function () {
                        $location.path('/search');
                    });
            }else{
                var newContact = {
                    name: vm.newContact.name,
                    phone: vm.newContact.phone
                }

                $http.post('api/contacts', newContact).then(function(){
                    $location.path('/search');
                });
      
            }
        };
        
        function postContact(newContact){
            $http.post('api/contacts', newContact).then(function(){
                $location.path('/search');
            });    
        }

        function show(){
            if($routeParams.id !== undefined){
                $http.get('api/contacts/' + $routeParams.id).then(function(result){
                    vm.newContact = result.data;
                    vm.edit = true;
                    })
            };

        }
    }
})();