 (function () {
    'use strict';

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {

        $routeProvider.when('/search', {
            templateUrl : 'app/search.html',
            controller : 'SearchCtrl',
            controllerAs : 'vm'
        }).when('/new/:id', {
            templateUrl : 'app/newContact.html',
            controller : 'NewContactCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app/newContact.html',
            controller : 'NewContactCtrl',
            controllerAs : 'vm'
        }).otherwise('/search');
    }

})();