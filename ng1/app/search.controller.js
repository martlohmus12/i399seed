(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', Ctrl);

    function Ctrl($http, modalService) {
        var vm = this;
        this.contacts = [];
        this.deleteContact = deleteContact;
        this.selected = false;

        init();

        function init(){
            $http.get('api/contacts').then(function (result){
                vm.contacts = [];
                for(var i = 0; i < result.data.length; i++){
                    vm.contacts.push(new contact(result.data[i]._id, result.data[i].name, result.data[i].phone));
                }
            });
        }

        function deleteContact(id){
            modalService.confirm().then(function(){
                return $http.delete('api/contacts/' + id);
                }).then(init);
        };

        function contact(id, name, phone){
            this._id = id;
            this.name = name;
            this.phone = phone;
            this.selected = false;
        }
    }

})();