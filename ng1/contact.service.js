'use strict';

const mongodb = require('mongodb');

const ObjectID = mongodb.ObjectID;

const url = 'mongodb://user1:user123456@ds129610.mlab.com:29610/i399';

class ContactService {

    getContacts() {
        var database;
        return mongodb.MongoClient.connect(url).then(db => {
            database = db;
            return Promise.resolve(db.collection('contacts').find().toArray());
        }).then(data => {
            this.closeDb(database);
            return Promise.resolve(data);
        }).catch(error => {
            this.closeDb(database);
            return Promise.reject(error)
        });
    }

    getContact(id) {
        var database;
        return mongodb.MongoClient.connect(url).then(db => {
            database = db;
            id = new ObjectID(id);
            return db.collection('contacts').findOne({ _id : id});
        }).then(data => {
            this.closeDb(database);
            return Promise.resolve(data);
        }).catch(error => {
            this.closeDb(database);
            return Promise.reject(error + 'no contact with id: ' + id);
        });
    }

    updateContact(contact) {
        var database;
        return mongodb.MongoClient.connect(url).then(db => {
            database = db;
            contact._id = new ObjectID(contact._id);
            return Promise.resolve(db.collection('contacts').updateOne({'_id': contact._id},contact));
        }).then(data => {
            this.closeDb(database);
            return Promise.resolve(data);
        }).catch(error => {
            this.closeDb(database);
            return Promise.reject(error);
        });
    }

    deleteContact(id) {
        var database;
        return mongodb.MongoClient.connect(url).then(db => {
            database = db;
            id = new ObjectID(id);
            return db.collection('contacts').deleteOne({ _id : id});
        }).then(data => {
            this.closeDb(database);
            return Promise.resolve(data);
        }).catch(error => {
            this.closeDb(database);
            return Promise.reject(error + 'no contact with id: ' + id);
        });
    }

    deleteContacts(ids) {
        var database;
        return mongodb.MongoClient.connect(url).then(db => {
            database = db;
            for(var i = 0; i < ids.length; i++){
                ids[i] = new ObjectID(ids[i]);
            }
            return Promise.resolve(db.collection('contacts').deleteMany({'_id': {$in: ids}}));
        }).then(data => {
            this.closeDb(database)
            return Promise.resolve(data);
        }).catch(error => {
            this.closeDb(database);
            //throw error;
            return Promise.reject(error);
        });
    }

    saveContact(contact) {
        var database;
        return mongodb.MongoClient.connect(url).then(db => {
            database = db;
            return db.collection('contacts').insertOne(contact);
        }).then(contact => {
            this.closeDb(database);
            return Promise.resolve(contact);
        }).catch(error => {
            this.closeDb(database);
            return Promise.reject(error);
        });
    }

    closeDb(database) {
    if (database) {
        database.close();
        }
    }
}

module.exports = ContactService;
