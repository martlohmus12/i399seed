'use strict';

class Contact {
    constructor(name, phone) {
        this.name = name;
        this.phone = phone;
    }

    static withId(id, name, phone) {
        let contact = new Contact(name, phone);
        contact._id = id;
        return contact;
    }
}

module.exports = Contact;