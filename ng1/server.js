'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const ContactService = require('./contact.service');
const dao = new ContactService();
const app = express();

app.use(bodyParser.json());

app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);
app.put('/api/contacts/:id', updateContact);
app.post('/api/contacts', saveContact);
app.post('/api/contacts/delete', deleteContacts);
app.delete('/api/contacts/:id', deleteContact);

app.listen(3000, () => console.log('Server is running on port 3000'));
                                
app.use(express.static('./'));
  
function saveContact(request, response) {
    dao.saveContact(request.body)
        .then(() => {
            response.end();
        }).catch( error => {
        response.json({error: error});
    });
}

function updateContact(request, response) {
    dao.updateContact(request.body)
        .then(contact => {
            response.json(contact);
        }).catch( error => {
        response.json({error: error});
    });
}

function deleteContact(request, response) {
    var id = request.params.id;
    dao.deleteContact(id)
        .then(() => {
            response.end();
        }).catch( error => {
        response.json({error: error});
    });
}

function deleteContacts(request, response) {
    var ids = request.body;
    dao.deleteContacts(ids)
        .then(() => {
            response.end();
        }).catch( error => {
        response.json({error: error});
    });
}

function getContact(request, response) {
    var id = request.params.id;
    dao.getContact(id)
        .then(contact => {
            response.json(contact);
        }).catch( error => {
        response.json({error: error});
    });
}

function getContacts(request, response) {
    dao.getContacts()
        .then(contacts => {
            response.json(contacts);
        });
}

